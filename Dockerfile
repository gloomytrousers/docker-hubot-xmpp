FROM node:alpine

LABEL org.opencontainers.image.title="hubot-xmpp"
LABEL org.opencontainers.image.description="Hubot with the XMPP adapter"
LABEL org.opencontainers.image.authors="Russ Odom <russ@gloomytrousers.co.uk>"
LABEL org.opencontainers.image.url="https://gitlab.com/gloomytrousers/docker-hubot-xmpp"

# Install OS packages
RUN apk add --update \
		redis \
		supervisor \
	&& \
	rm -rf /var/cache/apk/*

## Supervisor (run redis and hubot), a user, and a dir for the brain
COPY supervisord.conf /etc/supervisor/supervisord.conf
COPY run.sh /run.sh
COPY healthcheck.sh /healthcheck.sh
RUN install -d /etc/supervisor && \
	install -d /etc/supervisor/conf.d && \
	chmod a+x /run.sh /healthcheck.sh && \
	sed -i 's#^dir .*#dir /brain#' /etc/redis.conf && \
	sed -i 's#^daemonize .*#daemonize no#' /etc/redis.conf && \
	sed -i 's#^logfile .*#logfile ""#' /etc/redis.conf && \
	sed -i 's#^appendonly .*#appendonly yes#' /etc/redis.conf && \
	sed -i 's/^unixsocket/# unixsocket/' /etc/redis.conf && \
	chmod a+r /etc/redis.conf && \
	adduser -h /opt/hubot -G users -DS hubot && \
	mkdir /brain && \
	chown hubot:users /brain

# Volume containing Hubot's brain
VOLUME /brain

# Everything else can happen as a non-privileged user
USER hubot
WORKDIR /opt/hubot

# Bot name (can't be "hubot")
ENV BOTNAME Hal

# "yo hubot" installs a bunch of plugins by default; remove ones we don't want
ENV PLUGINS_REMOVE_DEFAULT hubot-google-images hubot-google-translate hubot-help hubot-heroku-keepalive hubot-maps hubot-pugme hubot-shipit

# Create the bot
RUN npm install \
		generator-hubot \
		yo && \
	node_modules/.bin/yo hubot \
		--name="$BOTNAME" \
		--owner="" \
		--description="A helpful XMPP robot" \
		--adapter=xmpp \
		--defaults && \
	npm remove \
		generator-hubot \
		yo && \
	if [ ! -z "$PLUGINS_REMOVE_DEFAULT" ]; then \
		npm remove $PLUGINS_REMOVE_DEFAULT && \
		for P in $PLUGINS_REMOVE_DEFAULT; do node -e "var plugins = $(cat /opt/hubot/external-scripts.json); plugins.splice(plugins.indexOf('$P'), 1); console.log(JSON.stringify(plugins));" > /opt/hubot/external-scripts.json; done; \
	fi && \
	rm -rf /opt/hubot/scripts /opt/hubot/hubot-scripts.json

# Extra plugins we want to add
ENV PLUGINS_ADD hubot-answers hubot-calculator hubot-dadbot hubot-decide hubot-even-better-help hubot-falsehoods hubot-frinkiac hubot-giphy hubot-has-no-idea hubot-open-weather-map hubot-podbaydoors hubot-remind-her hubot-simple-greeting hubot-tell hubot-thank-you

# Install Hubot plugins
RUN if [ ! -z "$PLUGINS_ADD" ]; then \
		npm install --save $PLUGINS_ADD && \
		for P in $PLUGINS_ADD; do node -e "var plugins = $(cat /opt/hubot/external-scripts.json); plugins.push('$P'); console.log(JSON.stringify(plugins));" > /opt/hubot/external-scripts.json; done; \
	fi

HEALTHCHECK --timeout=2s CMD ["/healthcheck.sh"]
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/supervisord.conf"]
