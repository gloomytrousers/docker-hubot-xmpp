# docker-hubot-xmpp

[![pipeline status](https://gitlab.com/gloomytrousers/docker-hubot-xmpp/badges/master/pipeline.svg)](https://gitlab.com/gloomytrousers/docker-hubot-xmpp/commits/master)

A Docker implementation of [Hubot](https://hubot.github.com/) with the [XMPP adapter](https://www.npmjs.com/package/hubot-xmpp), heavily modified from [another project](https://github.com/netmarkjp/docker-hubot-xmpp).

Contains various useful(?) plugins which, to be honest, are just personal preference - but they're easily amendable at build time.

**This project is only minimally maintained. I cannot guarantee continued support or ongoing updates. Offers to take over maintenance are welcome.**

## Run

A minimal command to run it:
```
docker run \
--volume hubot-brain:/brain \
--env TZ='Europe/London' \
--env HUBOT_XMPP_USERNAME=robot@example.com \
--env HUBOT_XMPP_PASSWORD=password \
--env HUBOT_XMPP_ROOMS=room@conference.example.com \
--env HUBOT_OPEN_WEATHER_MAP_APIKEY=xxxxxxxxxxxx \
--env HUBOT_OPEN_WEATHER_MAP_DEFAULT_CITIES=YourCity \
--name hubot \
gloomytrousers/hubot-xmpp
```

`HUBOT_XMPP_USERNAME` and `HUBOT_XMPP_PASSWORD` should be obvious. `HUBOT_XMPP_ROOMS` is a comma-separated list of room JIDs, and if any of them require a password, you can append to the JID like `room@conference.example.com:password`. Setting the right TZ ensures reminders are in your local timezone, not UTC.

Get a `HUBOT_OPEN_WEATHER_MAP_APIKEY` for free at [OpenWeatherMap](https://openweathermap.org/api). `HUBOT_OPEN_WEATHER_MAP_DEFAULT_CITIES` is a comma-separated list of place names.

Other environment variables are supported by some of the installed plugins, which are:
* [hubot-answers](https://www.npmjs.com/package/hubot-answers)
* [hubot-calculator](https://www.npmjs.com/package/hubot-calculator)
* ~~[hubot-dadbot](https://www.npmjs.com/package/hubot-dadbot)~~
* [hubot-decide](https://www.npmjs.com/package/hubot-decide)
* [hubot-diagnostics](https://www.npmjs.com/package/hubot-diagnostics)
* [hubot-even-better-help](https://www.npmjs.com/package/hubot-even-better-help)
* ~~[hubot-falsehoods](https://www.npmjs.com/package/hubot-falsehoods)~~
* [hubot-frinkiac](https://www.npmjs.com/package/hubot-frinkiac)
* [hubot-giphy](https://www.npmjs.com/package/hubot-giphy)
* [hubot-has-no-idea](https://www.npmjs.com/package/hubot-has-no-idea)
* [hubot-open-weather-map](https://www.npmjs.com/package/hubot-open-weather-map)
* [hubot-podbaydoors](https://www.npmjs.com/package/hubot-podbaydoors)
* [hubot-remind-her](https://www.npmjs.com/package/hubot-remind-her)
* [hubot-rules](https://www.npmjs.com/package/hubot-rules)
* ~~[hubot-simple-greeting](https://www.npmjs.com/package/hubot-simple-greeting)~~
* [hubot-tell](https://www.npmjs.com/package/hubot-tell)
* [hubot-thank-you](https://www.npmjs.com/package/hubot-thank-you)

The volume exposed as "/brain" is a Redis database which handles persistence of data between invocations - so it remembers reminders, for example.

Once it's running, the simplest way to find out what it can do is add it as a contact, or join the same conference room you configured it to join, and say "**hal help**".

## Build

Simples:
```
docker build --tag hubot-xmpp .
```

The robot's default name is "Hal" (most commands to the bot need to be prefixed with its name, case-insensitive). You can change this at build time the `BOTNAME` environment variable, e.g. `--env BOTNAME=Bender`. Note this can't be the value `hubot`, but any other name is fine.

The default build of Hubot installs some default scripts, most of which are removed in this build. You can change this with the `PLUGINS_REMOVE_DEFAULT` environment variable.

If you want to install your own set of scripts rather than the defaults, set `PLUGINS_ADD` to a space-separated list. There are [hundreds available](https://www.npmjs.com/search?q=keywords%3Ahubot-scripts) (and some of them even work!).
