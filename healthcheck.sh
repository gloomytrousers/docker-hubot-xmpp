#!/bin/sh

# Check all expected processes running
for P in supervisord redis-server node; do
	if [ $(pgrep -x $P | wc -l) -ne 1 ]; then
		echo "Healthcheck failed: $P not running"
		exit 1
	fi
done

# Check the Redis data, if it exists, is writable
# (early versions of this image ran as root, so we pick up this case)
if [ -f /brain/dump.rdb ] && [ ! -w /brain/dump.rdb ]; then
	echo "Healthcheck failed: /brain/dump.rdb not writable (try 'chown $(getent passwd hubot | cut -d: -f3,4) ...')"
	exit 1
fi
